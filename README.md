# NGINX with PHP7.2 and SSL

[NGINX](https://www.nginx.com) with [PHP](http://php.net) 7.2.9-fpm and [Let's Encrypt](https://letsencrypt.org) SSL configuration

## Important:
- Replace "##DOMAIN##" with your domain in following files:
    - mysite.conf
    - docker-compose.yml

## Defaults:
Code folder:

- /data/nginx/www

Web root folder:
- /data/nginx/www/public

Logs:
- /data/nginx/logs/

SSL Certificates:
- /etc/letsencrypt/live/##DOMAIN##/

# TODO
- Auto generate SSL Certificates with [Let's Encrypt](https://letsencrypt.org) [Certbot](https://certbot.eff.org)
- Add a simple server without SSL, just the (not so) good and old http://
- Add to compose:
    - Redis
    - PostgreSQL
    - SQLite
    - MySQL
    - MongoDB